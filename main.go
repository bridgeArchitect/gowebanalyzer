package main

import (
	"io/ioutil"
	"net/http"
	"net/smtp"
	"os"
	"strconv"
	"strings"
	"time"
)

/* constants for working */
const (
	urlAddress = "http://91.202.128.107:55555/6D/L4Y/index.html"
	searchRow  = "Нехай це свято наповнить ваші серця добром"
	findRow    = "Квітень 29,"
	errfile    = "/home/bridgearchitect/SP/Lab6/ServiceWebGo/error.log"
	messfile   = "/home/bridgearchitect/SP/Lab6/ServiceWebGo/mess.log"
	smtpServer = "smtp.gmail.com:587"
	host       = "smtp.gmail.com"
	sender     = "tomilo.art.2016@gmail.com"
	receiver   = "tomilo.art.forscience@gmail.com"
	password   = "Moskow2012"
)

func handleError(err error) {

	/* handle error */
	if err != nil {

		var (
			fileErr *os.File
		)

		/* write into error logfile */
		fileErr, _ = os.OpenFile(errfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		_, _ = fileErr.WriteString(time.Now().String() + ":" + err.Error() + "\n")

	}

}

func writeMessageLog(message string) {

	var (
		fileMess *os.File
	)

	/* write into message logfile */
	fileMess, _ = os.OpenFile(messfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	_, _ = fileMess.WriteString(time.Now().String() + ":" + message + "\n")

}

func writeMessageEmail(message string) {

	var (
		auth smtp.Auth
		err  error
		letter string
	)

	/* set up authentication information */
	auth = smtp.PlainAuth("", sender, password, host)

	/* create letter */
	letter = "From: " + sender + "\n" +
		"To: " + receiver + "\n" +
		"Subject: Web app \n\n" +
		message

	/* write email message */
	err = smtp.SendMail(smtpServer, auth, sender, []string{receiver}, []byte(letter))
	handleError(err)

}

func analyzeWeb() {

	var (
		resp     *http.Response
		err      error
		arrBytes []byte
		rowHtml  string
		ind      int
		year     int
	)

	/* receive response from web page */
	resp, err = http.Get(urlAddress)
	handleError(err)

	/* check if response exists */
	if err != nil {
		/* stop execution */
		return
	}

	/* read response as slice of bytes */
	arrBytes, err = ioutil.ReadAll(resp.Body)
	handleError(err)

	/* close body of response */
	err = resp.Body.Close()
	handleError(err)

	/* convert slice of bytes into row */
	rowHtml = string(arrBytes)

	/* check existence of line */
	ind = strings.Index(rowHtml, searchRow)
	if ind == -1 {
		/* write message */
		writeMessageLog("The necessary row is disappeared!")
		writeMessageEmail("The necessary row is disappeared!")
	}

	/* check existence of additional line */
	ind = strings.Index(rowHtml, findRow)
	if ind != -1 {
		/* receive year from string */
		year, err = strconv.Atoi(rowHtml[ind + 19:ind + 23])
		handleError(err)
		/* check if this year multiples of 3 */
		if year % 3 != 0 {
			/* write message */
			writeMessageLog("Year does not multiple of 3!")
			writeMessageEmail("Year does not multiple of 3!")
		}
	}

}

func main() {

	/* infinity loop */
	for true {
		/* make analysis */
		analyzeWeb()
		/* sleep process */
		time.Sleep(10 * time.Second)
	}

}
